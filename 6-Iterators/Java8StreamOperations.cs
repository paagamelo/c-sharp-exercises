using System;
using System.Collections.Generic;

namespace Iterators {

    public static class Java8StreamOperations
    {
        public static void ForEach<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            foreach(TAny element in sequence)
            {
                consumer(element);
            }
        }

        public static IEnumerable<TAny> Peek<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            sequence.ForEach(consumer);
            return sequence;
        }

        public static IEnumerable<TOther> Map<TAny, TOther>(this IEnumerable<TAny> sequence, Func<TAny, TOther> mapper)
        {
            var outputSequence = new List<TOther>();
            foreach(TAny element in sequence)
            {
                outputSequence.Add(mapper(element));
            }
            return outputSequence;
        }

        public static IEnumerable<TAny> Filter<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var outputSequence = new List<TAny>();
            foreach(TAny element in sequence)
            {
                if (consumer(element))
                {
                    outputSequence.Add(element);
                }
            }
            return outputSequence;
        }

        public static IEnumerable<Tuple<int, TAny>> Indexed<TAny>(this IEnumerable<TAny> sequence)
        {
            var outputSequence = new List<Tuple<int, TAny>>();
            var index = 0;
            foreach(TAny element in sequence)
            {
                outputSequence.Add(new Tuple<int, TAny>(index, element));
                index++;
            }
            return outputSequence;
        }

        public static TOther Reduce<TAny, TOther>(this IEnumerable<TAny> sequence, TOther seed, Func<TOther, TAny, TOther> reducer)
        {
            TOther result = seed;
            foreach(TAny element in sequence)
            {
                result = reducer(result, element);
            }
            return result;
        }

        public static IEnumerable<TAny> SkipWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var outputSequence = new List<TAny>();
            var iterator = sequence.GetEnumerator();
            while (consumer(iterator.Current))
            {
                iterator.MoveNext();
            }
            do
            {
                outputSequence.Add(iterator.Current);
            } while (iterator.MoveNext());
            return outputSequence;
        }

        public static IEnumerable<TAny> SkipSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            var outputSequence = new List<TAny>();
            foreach(TAny element in sequence)
            {
                if(count > 0)
                {
                    count--;
                }
                else
                {
                    outputSequence.Add(element);
                }
            }
            return outputSequence;
        }

        public static IEnumerable<TAny> TakeWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var outputSequence = new List<TAny>();
            var iterator = sequence.GetEnumerator();
            while(consumer(iterator.Current))
            {
                outputSequence.Add(iterator.Current);
                iterator.MoveNext();    //if(..) break;
            }
            return outputSequence;
        }

        public static IEnumerable<TAny> TakeSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            var outputSequence = new List<TAny>();
            var iterator = sequence.GetEnumerator();
            while(count > 0)
            {
                outputSequence.Add(iterator.Current);
                count--;
                iterator.MoveNext();    //ugly!!
            }
            return outputSequence;
        }

        public static IEnumerable<int> Integers(int start)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<int> Integers()
        {
            return Integers(0);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return Integers().TakeSome(count);
        }

    }

}