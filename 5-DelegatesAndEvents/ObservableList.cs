﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        private List<TItem> list = new List<TItem>();

        public IEnumerator<TItem> GetEnumerator()
        {
            for(int curr = 0; curr <= list.Count; curr++)
            {
                yield return list[curr];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            list.Add(item);
            if (ElementInserted != null)
            {
                ElementInserted(this, item, list.IndexOf(item));
            }
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(TItem item)
        {
            return list.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            if (ElementRemoved != null)
            {
                ElementRemoved(this, item, list.IndexOf(item));
            }
            return list.Remove(item);
        }

        public int Count
        {
            get { return list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public int IndexOf(TItem item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            list.Insert(index, item);
            if(ElementInserted != null)
            {
                ElementInserted(this, item, index);
            }
        }

        public void RemoveAt(int index)
        {
            if (ElementRemoved != null)
            {
                ElementRemoved(this, list[index], index);
            }
            list.RemoveAt(index);
        }

        public TItem this[int index]
        {
            get { return list[index]; }
            set
            {
                if (ElementChanged != null)
                {
                    ElementChanged(this, value, list[index], index);
                }
                list[index] = value;
            }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return list.ToString();
        }

        public override bool Equals(object obj)
        {
            return list.Equals(obj);
        }

        public override int GetHashCode()
        {
            return list.GetHashCode();
        }
    }

}