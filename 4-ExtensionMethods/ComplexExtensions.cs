﻿using System;

namespace ExtensionMethods {

    public static class ComplexExtensions
    {
        public static IComplex Add(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
        }

        public static IComplex Subtract(this IComplex c1, IComplex c2)
        {
            return new Complex(c1.Real - c2.Real, c1.Imaginary - c2.Imaginary);
        }

        public static IComplex Multiply(this IComplex c1, IComplex c2)
        {
            return new Complex((c1.Real * c2.Real) - (c1.Imaginary * c2.Imaginary), (c1.Real * c2.Real) + (c1.Imaginary * c2.Imaginary));
        }

        public static IComplex Divide(this IComplex c1, IComplex c2)
        {
            var divisor = Math.Pow(c2.Modulus, 2);
            return new Complex((c1.Real * c2.Real + c1.Imaginary * c2.Imaginary) / divisor, (c1.Imaginary * c2.Real - c1.Real * c2.Imaginary) / divisor);
        }

        public static IComplex Conjugate(this IComplex c1)
        {
            return new Complex(c1.Real, -c1.Imaginary);
        }

        public static IComplex Reciprocal(this IComplex c1)
        {
            return new Complex(c1.Real / Math.Pow(c1.Modulus, 2), -c1.Imaginary / Math.Pow(c1.Modulus, 2));
        }
    }

}