﻿using System;

namespace ExtensionMethods
{
    internal class Complex : IComplex
    {
        private double re;
        private double im;
        private const double tolerance = 0.00001;

        public Complex(double re, double im)
        {
            this.re = re;
            this.im = im;
        }

        public bool Equals(IComplex other)
        {
            return (this.Real - other.Real) < tolerance && (this.Imaginary - other.Imaginary) < tolerance;
        }

        public double Real
        {
            get { return this.re; }
        }

        public double Imaginary
        {
            get { return this.im; }
        }
        public double Modulus
        {
            get { return Math.Sqrt(Math.Pow(this.Real, 2) + Math.Pow(this.Imaginary, 2)); }
        }
        public double Phase
        {
            get { return Math.Atan2(this.Imaginary, this.Real); }
        }

        public override string ToString()
        {
            return $"Complex: {this.Real} + i{this.Imaginary}";
        }

        public override bool Equals(object obj)
        {
            if(obj is IComplex)
            {
                return this.Equals(obj);
            }
            return false;
        }

        public int HashCode
        { 
            get { return base.GetHashCode(); }
        }

    }
}